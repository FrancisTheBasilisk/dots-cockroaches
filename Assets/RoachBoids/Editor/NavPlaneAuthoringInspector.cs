﻿using UnityEngine;
using UnityEditor;

//Placeholder for the inspector part of the NavPlaneAuthoring editor
//As the OnSceneGUI part got too big, it was moved to another file

public partial class NavPlaneAuthoringEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }
}