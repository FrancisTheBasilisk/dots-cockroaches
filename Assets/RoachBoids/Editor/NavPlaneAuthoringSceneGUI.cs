﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;
using System.Linq;

//Editor script that draws handles for easy NavPlane manipulation
//Hold LeftAlt for symmetric scaling, double-click a handle to set pivot

[CustomEditor(typeof(NavPlaneAuthoring))]
public partial class NavPlaneAuthoringEditor : Editor
{
    const float handleSizeMultiplier = 0.01f;

    NavPlaneAuthoring m_target;
    private bool symmetricScaling = false;
    private int[] scaleGizmoControlIDs = new int[9];

    private void Awake()
    {
        m_target = target as NavPlaneAuthoring;
    }

    private void OnEnable()
    {
        symmetricScaling = false;
    }

    private void OnSceneGUI()
    {
        if (HandleSceneGUIEvents()) Event.current.Use();

        DoScaleGizmo(QuadPosition.TopLeft);
        DoScaleGizmo(QuadPosition.TopMiddle);
        DoScaleGizmo(QuadPosition.TopRight);
        DoScaleGizmo(QuadPosition.MiddleLeft);
        DoScaleGizmo(QuadPosition.Center);
        DoScaleGizmo(QuadPosition.MiddleRight);
        DoScaleGizmo(QuadPosition.BottomLeft);
        DoScaleGizmo(QuadPosition.BottomMiddle);
        DoScaleGizmo(QuadPosition.BottomRight);

        Undo.RecordObject(m_target, "NavPlane scale change");
        Undo.RecordObject(m_target.transform, "NavPlane scale change");
    }

    private bool HandleSceneGUIEvents()
    {
        Event currentEvent = Event.current;

        if (currentEvent.type == EventType.KeyDown && currentEvent.keyCode == KeyCode.LeftAlt)
        {
            symmetricScaling = true;
            return true;
        }
        if (currentEvent.type == EventType.KeyUp && currentEvent.keyCode == KeyCode.LeftAlt)
        {
            symmetricScaling = false;
            return true;
        }
        if(currentEvent.type == EventType.MouseDown && currentEvent.clickCount == 2
            && scaleGizmoControlIDs.Contains(HandleUtility.nearestControl)) 
        {
            QuadPosition selectedHandle = (QuadPosition)Array.IndexOf(scaleGizmoControlIDs, HandleUtility.nearestControl);
            m_target.ChangePivotPosition(selectedHandle);
            return true;
        }
        return false;
    }

    private void DoScaleGizmo(QuadPosition quadPosition)
    {
        if (m_target.pivotPosition == quadPosition) return;

        int controlID = GUIUtility.GetControlID(FocusType.Passive);
        scaleGizmoControlIDs[(int)quadPosition] = controlID;

        Vector3 localOffset = quadPosition.GetOffset();
        Vector3 scaledOffset = localOffset;
        scaledOffset.x *= m_target.width;
        scaledOffset.y *= m_target.height;

        Vector3 worldGizmoPos = m_target.planePosition + m_target.transform.TransformDirection(scaledOffset);
        float handleSize = Vector3.Distance(SceneView.lastActiveSceneView.camera.transform.position, worldGizmoPos) * handleSizeMultiplier;
        Vector3 newWorldGizmoPos = Handles.FreeMoveHandle(controlID, worldGizmoPos, Quaternion.identity, handleSize, Vector3.one, Handles.DotHandleCap);

        if (quadPosition == QuadPosition.Center) return;

        Vector3 change = m_target.transform.InverseTransformDirection(newWorldGizmoPos - worldGizmoPos);
        change.z = 0;
        if (quadPosition.LiesOnXAsis()) change.y = 0;
        if (quadPosition.LiesOnYAsis()) change.x = 0;

        if(change.sqrMagnitude > 0)
        {
            float deltaX = change.x * (localOffset.x * 2 - 1);
            float deltaY = change.y * (localOffset.y * 2 - 1);

            Vector3 offsetFromPivot = quadPosition.GetOffset() - m_target.pivotPosition.GetOffset();
            
            if (!symmetricScaling)
            {
                Vector3 transformCorrection = change;
                transformCorrection.x *= 1 - Mathf.Abs(offsetFromPivot.x);
                transformCorrection.y *= 1 - Mathf.Abs(offsetFromPivot.y);

                m_target.transform.position += m_target.transform.TransformDirection(transformCorrection);
            }
            else if (m_target.pivotPosition != QuadPosition.Center)
            {
                if (offsetFromPivot.x == 0) deltaX = 0;
                else if (offsetFromPivot.y == 0) deltaY = 0;
            }

            m_target.width += deltaX;
            m_target.height += deltaY;
        }
    }    
}
