﻿using UnityEngine;
using System.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityRandom = UnityEngine.Random;
using Unity.Transforms;

//A temporary script to spawn a bunch of boids on a desired plane
//The plane ID in the blob asset (for now) is determined by order in hierarchy of NavPlaneAuthoring components
public class SpawnBoids : MonoBehaviour
{
    [SerializeField] int planeID = 0;
    [SerializeField] float boidSpeed = 0.05f;
    [SerializeField] int boidCount = 1000;
    [SerializeField] GameObject prefab = null;

    [SerializeField] bool randomHeading = true;
    [SerializeField] Vector2 defaultHeading = Vector2.right;
    private void Start()
    {
        ref NavPlane plane = ref NavMapSystem.Map.Value.planes[planeID];

        EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        Entity entityPrefab;

        using (BlobAssetStore blobAssetStore = new BlobAssetStore())
        {
            GameObjectConversionSettings settrings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blobAssetStore);
            entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settrings);
        }

        for(int i = 0; i < boidCount; i++)
        {
            Entity entity = entityManager.Instantiate(entityPrefab);

#if UNITY_EDITOR
            entityManager.SetName(entity, "Boid");
#endif

            entityManager.AddComponentData(entity, new PlanarPosition()
            {
                planeID = planeID,
                Value = new float2(UnityRandom.Range(0, plane.width), UnityRandom.Range(0, plane.height))
            });

            entityManager.AddComponentData(entity, new PlanarHeading()
            {
                Value = randomHeading ? UnityRandom.insideUnitCircle : defaultHeading
            });

            entityManager.AddComponentData(entity, new Boid()
            {
                speed = boidSpeed
            });

            //Remove standard transform components
            entityManager.RemoveComponent<Translation>(entity);
            entityManager.RemoveComponent<Rotation>(entity);
        }
    }
}
