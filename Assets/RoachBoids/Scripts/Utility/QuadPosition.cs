﻿using UnityEngine;
using System.Collections;

//This enum is used in editor authoring scripts and represents the 9 handle positions used to manipulate a plane
public enum QuadPosition
{
    TopLeft, TopMiddle, TopRight,
    MiddleLeft, Center, MiddleRight,
    BottomLeft, BottomMiddle, BottomRight
}

public static class QuadPositionExtentions
{
    public static Vector3 GetOffset(this QuadPosition quadPos)
    {
        switch(quadPos)
        {
            case QuadPosition.TopLeft: return new Vector3(0, 1, 0);
            case QuadPosition.TopMiddle: return new Vector3(0.5f, 1, 0);
            case QuadPosition.TopRight: return new Vector3(1, 1, 0);
            case QuadPosition.MiddleLeft: return new Vector3(0, 0.5f, 0);
            case QuadPosition.Center: return new Vector3(0.5f, 0.5f, 0);
            case QuadPosition.MiddleRight: return new Vector3(1, 0.5f, 0);
            case QuadPosition.BottomLeft: return new Vector3(0, 0, 0);
            case QuadPosition.BottomMiddle: return new Vector3(0.5f, 0, 0);
            case QuadPosition.BottomRight: return new Vector3(1, 0, 0);
            default: return Vector3.zero;
        }
    }

    public static bool LiesOnXAsis(this QuadPosition quadPos)
    {
        switch(quadPos)
        {
            case QuadPosition.MiddleLeft:
            case QuadPosition.Center:
            case QuadPosition.MiddleRight:
                return true;
            default: return false;
        }
    }

    public static bool LiesOnYAsis(this QuadPosition quadPos)
    {
        switch (quadPos)
        {
            case QuadPosition.TopMiddle:
            case QuadPosition.Center:
            case QuadPosition.BottomMiddle:
                return true;
            default: return false;
        }
    }
}
