﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

//Static methods to help draw gizmos/handles in the scene view
//Can be called from either OnSceneGUI() on OnDrawGizmos()
public static class GizmoUtility
{
    public static readonly Color activeGizmoColor = new Color(0.07139516f, 1, 0, 0.8f);
    public static readonly Color inactiveGizmoColor = new Color(0.3726415f, 0.3849427f, 1, 0.5f);

    static Mesh m_quad;

    public static Mesh Quad
    {
        get
        {
            if (m_quad == null) CreateQuad();
            return m_quad;
        }
    }

    private static void CreateQuad()
    {
        m_quad = new Mesh();

        Vector3[] vertices = new Vector3[4]
        {
            new Vector3(0, 0, 0),
            new Vector3(1, 0, 0),
            new Vector3(0, 1, 0),
            new Vector3(1, 1, 0)
        };
        m_quad.vertices = vertices;


        int[] triangles = new int[6]
        {
            0, 2, 1,
            2, 3, 1
        };
        m_quad.triangles = triangles;

        Vector3[] normals = new Vector3[4]
        {
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward,
            -Vector3.forward
        };
        m_quad.normals = normals;
    }

    static Texture2D m_localPortalLine;
    public static Texture2D LocalPortalLineTexture
    {
        get
        {
            if (m_localPortalLine == null) m_localPortalLine = CreateThickLine(10, Color.magenta);
            return m_localPortalLine;
        }
    }

    static Texture2D m_remotePortalLine;
    public static Texture2D RemotePortalLineTexture
    {
        get
        {
            if (m_remotePortalLine == null) m_remotePortalLine = CreateThickLine(30, Color.blue);
            return m_remotePortalLine;
        }
    }

    private static Texture2D CreateThickLine(int thickness, Color color)
    {
        Texture2D lineTexture = new Texture2D(1, thickness);
        Color[] colors = new Color[thickness];
        for (int i = 0; i < colors.Length; i++) colors[i] = color;
        lineTexture.SetPixels(colors);
        lineTexture.Apply();
        return lineTexture;
    }

    public static void DrawGrid(Vector3 position, Quaternion rotation, float rectWidth, float rectHeight, float cellSize)
    {
#if UNITY_EDITOR
        Matrix4x4 localToWorld = Matrix4x4.TRS(position, rotation, Vector3.one);

        int cellsX = Mathf.FloorToInt(rectWidth / cellSize);
        int cellsY = Mathf.FloorToInt(rectHeight / cellSize);

        for(int x = 1; x <= cellsX; x++)
        {
            float localX = x * cellSize;
            Vector3 start = new Vector3(localX, 0, 0);
            Vector3 end = new Vector3(localX, rectHeight, 0);
            Handles.DrawLine(localToWorld.MultiplyPoint(start), localToWorld.MultiplyPoint(end));
        }

        for (int y = 1; y <= cellsY; y++)
        {
            float localY = y * cellSize;
            Vector3 start = new Vector3(0, localY, 0);
            Vector3 end = new Vector3(rectWidth, localY, 0);
            Handles.DrawLine(localToWorld.MultiplyPoint(start), localToWorld.MultiplyPoint(end));
        }
#endif
    }
}
