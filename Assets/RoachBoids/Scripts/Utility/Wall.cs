﻿//This enum represents the 4 walls of a plane on which portals are located
public enum Wall
{
    UP, DOWN, LEFT, RIGHT
}

public static class WallExtentions
{
    public static Wall GetOpposite(this Wall wall)
    {
        switch(wall)
        {
            case Wall.UP: return Wall.DOWN;
            case Wall.DOWN: return Wall.UP;
            case Wall.LEFT: return Wall.RIGHT;
            case Wall.RIGHT: return Wall.LEFT;
        }
        return default;
    }
}
