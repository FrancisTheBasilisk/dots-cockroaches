﻿using Unity.Entities;
using Unity.Mathematics;

//Burst compilable static utility class for portals between planes
public static class PortalUtility
{
    #region positions

    //Converts 1D position on a Wall to a 2D position on a plane
    public static float2 GetPortalPositionOnPlane(Wall wall, float position, float planeWidth, float planeHeight)
    {
        switch(wall)
        {
            case Wall.LEFT: return new float2(0, position);
            case Wall.RIGHT: return new float2(planeWidth, position);
            case Wall.DOWN: return new float2(position, 0);
            case Wall.UP: return new float2(position, planeHeight);
        }
        return default;
    }

    //Projects 2D position on a plane into a 1D position on a Wall
    public static float ProjectOntoWall(float2 planePosition, Wall wall)
    {
        switch(wall)
        {
            case Wall.UP:
            case Wall.DOWN: return planePosition.x;
            case Wall.LEFT:
            case Wall.RIGHT: return planePosition.y;
        }
        return default;
    }

    public static ref BlobArray<NavPortal> GetPortalsOnWall(Wall wall, ref NavPlane plane)
    {
        switch (wall)
        {
            case Wall.UP: return ref plane.portalsUp;
            case Wall.DOWN: return ref plane.portalsDown;
            case Wall.LEFT: return ref plane.portalsLeft;
            case Wall.RIGHT: return ref plane.portalsRight;
        }
        return ref plane.portalsUp;
    }
    #endregion

    #region findPortal
    //Project a 2D position onto a wall and check if the projection aligns with ANY portal
    public static bool PositionAlignsWithPortal(Wall wall, float2 position, ref NavPlane plane)
    {
        float projectedPosition = ProjectOntoWall(position, wall);
        ref BlobArray<NavPortal> portalsOnWall = ref GetPortalsOnWall(wall, ref plane);

        for (int i = 0; i < portalsOnWall.Length; i++)
        {
            ref NavPortal portal = ref portalsOnWall[i];
            if (projectedPosition >= portal.positionOnThisPlane && projectedPosition <= portal.positionOnThisPlane + portal.length)
            {
                return true;
            }
        }
        return false;
    }

    //Project a 2D position onto a wall and find the portal it aligns with
    public static bool TryFindAlignedPortal(Wall wall, float2 position, ref NavPlane plane,
        out NavPortal foundPortal)
    {
        float projectedPosition = ProjectOntoWall(position, wall);
        ref BlobArray<NavPortal> portalsOnWall = ref GetPortalsOnWall(wall, ref plane);

        for (int i = 0; i < portalsOnWall.Length; i++)
        {
            ref NavPortal portal = ref portalsOnWall[i];
            if (projectedPosition >= portal.positionOnThisPlane && projectedPosition <= portal.positionOnThisPlane + portal.length)
            {
                foundPortal = portal;
                return true;
            }
        }
        foundPortal = default;
        return false;
    }
    #endregion
}
