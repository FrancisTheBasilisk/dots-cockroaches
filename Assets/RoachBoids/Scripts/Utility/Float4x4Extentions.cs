﻿using Unity.Mathematics;

public static class Float4x4Extentions
{
    public static float3 MultiplyPoint(this float4x4 a, float3 b)
    {
        return (a.c0 * b.x + a.c1 * b.y + a.c2 * b.z + a.c3).xyz;
    }

    public static float3 MultiplyPoint(this float4x4 a, float2 b)
    {
        return (a.c0 * b.x + a.c1 * b.y + a.c3).xyz;
    }

    public static float3 MultiplyVector(this float4x4 a, float3 b)
    {
        return (a.c0 * b.x + a.c1 * b.y + a.c2 * b.z).xyz;
    }

    public static float3 MultiplyVector(this float4x4 a, float2 b)
    {
        return (a.c0 * b.x + a.c1 * b.y).xyz;
    }
}
