﻿using Unity.Mathematics;

public static class MathUtility
{
    public static float EaseOutQuad(float x)
    {
        return 1 - (1 - x) * (1 - x);
    }
}
