﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

//This component represents a 2D location on one of the planes of the NavMap

[Serializable]
public struct PlanarPosition : IComponentData
{
    public int planeID;
    public float2 Value;
}
