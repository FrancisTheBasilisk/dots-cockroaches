﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

//This is a BlobAsset representation of the space in which boids will move
//The map consists of several planes connected by portals
public struct NavMap
{
    public BlobArray<NavPlane> planes;
    public BoidSettings settings;
}

public struct BoidSettings
{
    public float cellSize; //How big is a square cell in which all boids are considered neighbors?
    public float neighborAvoidanceRadius;
    public float wallAvoidanceDistance;

    public float cohesionWeight; //The weight of boids' cohesion behaviour
    public float alignmentWeight; //The weight of boids' alignment behaviour
    public float avoidanceWeight; //The weight of boids' neighbor avoidance behaviour
    public float wallAvoidanceWeight; //The weight of boids' wall avoidance behaviour
}

public struct NavPlane
{
    public float3 position; //Plane's position in 3D space
    public quaternion rotation; //Plane's rotation in 3D space
    public float4x4 planeToWorld; //Transformation matrix from local space to 3D world space
    public float width;
    public float height;

    public BlobArray<NavPortal> portalsUp; //Portals on the upper wall
    public BlobArray<NavPortal> portalsDown; //Portals on the lower wall
    public BlobArray<NavPortal> portalsLeft; //Portals on the left wall
    public BlobArray<NavPortal> portalsRight; //Portals on the right wall
}

public struct NavPortal
{
    public int targetPlaneID; //Index of the target plane in the planes array
    public float length; //1D-end-position - 1D-start-position = length

    public Wall wallOnThisPlane; //the wall on which this portal is located
    public float positionOnThisPlane; //1-dimensional position on this plane's wall

    public float positionOnTargetPlane; //1-dimensional position on the wall of a target plane
}


