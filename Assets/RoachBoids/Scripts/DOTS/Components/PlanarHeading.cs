﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

//This component represents a 2D heading direction

[Serializable]
public struct PlanarHeading : IComponentData
{
    public float2 Value;
}
