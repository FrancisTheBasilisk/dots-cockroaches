﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using static Unity.Mathematics.math;

//Updates the PlanarHeading value of a boid

public class BoidHeadingSystem : SystemBase
{
    //Query used for calcucating the number of boids
    EntityQuery boidQuery;
    
    //Boid id, position and heading copied for random access
    struct BoidData
    {
        public Entity entity;
        public float2 position;
        public float2 heading;
    }

    //Position with the same hash value are considered neighboring positions
    private static int HashPosition(PlanarPosition planarPosition, float cellSize)
    {
        return (int)math.hash(new int3((int2)floor(planarPosition.Value / cellSize), planarPosition.planeID));
    }

    protected override void OnUpdate()
    {
        BlobAssetReference<NavMap> map = NavMapSystem.Map;
        int boidCount = boidQuery.CalculateEntityCount();

        //Boids stored under the same hash value are considered naighbors
        NativeMultiHashMap<int, BoidData> hashMap = new NativeMultiHashMap<int, BoidData>(boidCount, Allocator.TempJob);
        var parallelHashMap = hashMap.AsParallelWriter();

        //Add all boids into the hashmap
        JobHandle hashPositionsJob = Entities.WithAll<Boid>().ForEach((Entity entity, in PlanarPosition planarPosition, in PlanarHeading planarHeading) =>
        {
            float cellSize = map.Value.settings.cellSize;
            int hash = HashPosition(planarPosition, cellSize);

            BoidData cellData = new BoidData()
            {
                entity = entity,
                position = planarPosition.Value,
                heading = planarHeading.Value
            };

            parallelHashMap.Add(hash, cellData);
        }).ScheduleParallel(Dependency);

        //Calculate new boid headings when the hash job has finished
        JobHandle boidJob = Entities
            .WithAll<Boid>()
            .WithReadOnly(hashMap)
            .ForEach((Entity entity, ref PlanarHeading heading, in PlanarPosition position) =>
        {
            ref BoidSettings settings = ref map.Value.settings;
            ref NavPlane plane = ref map.Value.planes[position.planeID];
            
            int hash = HashPosition(position, settings.cellSize);

            BoidData neighbor;
            NativeMultiHashMapIterator<int> iterator;

            float2 neighborPositionsSum = float2(0, 0);
            float2 neighborHeadingsSum = 0;
            float2 tooClosePositionsSum = float2(0, 0);
            int neighborCount = 0;
            int tooCloseCount = 0;

            //Iterate throug all neighbors
            if (hashMap.TryGetFirstValue(hash, out neighbor, out iterator))
            {
                do
                {
                    if (neighbor.entity == entity) continue;

                    neighborCount++;
                    neighborPositionsSum += neighbor.position;
                    neighborHeadingsSum += neighbor.heading;
                    if (distance(position.Value, neighbor.position) < settings.neighborAvoidanceRadius)
                    {
                        tooCloseCount++;
                        tooClosePositionsSum += neighbor.position;
                    }
                } while (hashMap.TryGetNextValue(out neighbor, ref iterator));
            }

            //Does a new heading need to be calculated by normalizing the sum?
            //Boids with no neighbors that aren't avoiding walls don't need to change their heading
            bool changeHeading = false;
            float2 resultingSum = new float2(0, 0);

            if (neighborCount > 1)
            {
                changeHeading = true;

                //Add cohesion (staying together) result to the sum
                float2 cohesionResult = math.normalizesafe((neighborPositionsSum / neighborCount) - position.Value);
                resultingSum += cohesionResult * settings.cohesionWeight;

                //Add neighbor avoidance result to the sum
                if (tooCloseCount > 0)
                {
                    float2 avoidanceResult = math.normalizesafe(position.Value - (tooClosePositionsSum / tooCloseCount));
                    resultingSum += avoidanceResult * settings.avoidanceWeight;
                }

                //Add neighbor alignment result to the sum
                float2 alignmentResult = normalize(neighborHeadingsSum / neighborCount);
                resultingSum += alignmentResult * settings.alignmentWeight;
            }

            float leftWallDistance = position.Value.x;
            float rightWallDistance = plane.width - position.Value.x;
            float lowerWallDistance = position.Value.y;
            float upperWallDistance = plane.height - position.Value.y;

            float wallAvoidanceDistance = settings.wallAvoidanceDistance;

            //If a boid too close to a wall but NOT near a portal, steer away from wall
            if(leftWallDistance < wallAvoidanceDistance && !PortalUtility.PositionAlignsWithPortal(Wall.LEFT, position.Value, ref plane))
            {
                changeHeading = true;
                resultingSum += new float2(1, 0) * WallAvoidanceMultiplier(leftWallDistance, wallAvoidanceDistance) * settings.wallAvoidanceWeight;
            }
            if (rightWallDistance < wallAvoidanceDistance && !PortalUtility.PositionAlignsWithPortal(Wall.RIGHT, position.Value, ref plane))
            {
                changeHeading = true;
                resultingSum += new float2(-1, 0) * WallAvoidanceMultiplier(rightWallDistance, wallAvoidanceDistance) * settings.wallAvoidanceWeight;
            }
            if (lowerWallDistance < wallAvoidanceDistance && !PortalUtility.PositionAlignsWithPortal(Wall.DOWN, position.Value, ref plane))
            {
                changeHeading = true;
                resultingSum += new float2(0, 1) * WallAvoidanceMultiplier(lowerWallDistance, wallAvoidanceDistance) * settings.wallAvoidanceWeight;
            }
            if (upperWallDistance < wallAvoidanceDistance && !PortalUtility.PositionAlignsWithPortal(Wall.UP, position.Value, ref plane))
            {
                changeHeading = true;
                resultingSum += new float2(0, -1) * WallAvoidanceMultiplier(upperWallDistance, wallAvoidanceDistance) * settings.wallAvoidanceWeight;
            }

            if (changeHeading) heading.Value = math.normalizesafe(resultingSum);
        }).ScheduleParallel(hashPositionsJob);

        //Dispose of the hash map when the boid job has finished
        Dependency = hashMap.Dispose(boidJob);
    }

    private static float WallAvoidanceMultiplier(float distanceToWall, float maxDistance)
    {
        return MathUtility.EaseOutQuad(maxDistance - distanceToWall);
    }

    protected override void OnCreate()
    {
        boidQuery = GetEntityQuery(new EntityQueryDesc()
        {
            All = new[]
            {
                ComponentType.ReadOnly<Boid>(),
                ComponentType.ReadOnly<PlanarPosition>(),
                ComponentType.ReadWrite<PlanarHeading>()
            }
        });

        RequireForUpdate(boidQuery);
    }
}