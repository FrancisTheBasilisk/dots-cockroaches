﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

//This system moves the boids forward according to their heading and handles teleportation between planes

public class BoidMoveSystem : SystemBase
{
    protected override void OnUpdate()
    {
        BlobAssetReference<NavMap> map = NavMapSystem.Map;

        Entities.ForEach((Entity entity, ref PlanarPosition position, in PlanarHeading heading, in Boid boid) =>
        {
            ref NavPlane plane = ref map.Value.planes[position.planeID];
            float2 newPosition = position.Value + heading.Value * boid.speed;

            //Teleport to another plane if went through a portal
            if (newPosition.y >= plane.height && TryTeleport(Wall.UP, newPosition, in map, ref position, ref plane)) return;
            if (newPosition.y <= 0 && TryTeleport(Wall.DOWN, newPosition, in map, ref position, ref plane)) return;
            if (newPosition.x <= 0 && TryTeleport(Wall.LEFT, newPosition, in map, ref position, ref plane)) return;
            if (newPosition.x >= plane.width && TryTeleport(Wall.RIGHT, newPosition, in map, ref position, ref plane)) return;

            position.Value = math.clamp(newPosition, new float2(0, 0), new float2(plane.width, plane.height));
        }).ScheduleParallel();  
    }

    private static bool TryTeleport(Wall wall, float2 newPosition, in BlobAssetReference<NavMap> map, ref PlanarPosition position, ref NavPlane plane)
    {
        NavPortal alignedPortal;
        if(PortalUtility.TryFindAlignedPortal(wall, newPosition, ref plane, out alignedPortal))
        {
            Teleport(in map, ref position, in alignedPortal);
            return true;
        }
        return false;
    }

    private static void Teleport(in BlobAssetReference<NavMap> map, ref PlanarPosition position, in NavPortal portal)
    {
        ref NavPlane targetPlane = ref map.Value.planes[portal.targetPlaneID];
        position.planeID = portal.targetPlaneID;

        float offset = PortalUtility.ProjectOntoWall(position.Value, portal.wallOnThisPlane) - portal.positionOnThisPlane;
        position.Value = PortalUtility.GetPortalPositionOnPlane(portal.wallOnThisPlane.GetOpposite(), portal.positionOnTargetPlane + offset, targetPlane.width, targetPlane.height);
    }
}