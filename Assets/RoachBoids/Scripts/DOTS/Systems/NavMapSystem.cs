﻿using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

//This system takes the data from NavMapAuthoring and NavPlaneAuthoring components
//and bakes it into a BlobAsset that can be assessed from any job via the static Map property

[UpdateInGroup(typeof(InitializationSystemGroup))]
public class NavMapSystem : SystemBase
{
    private static BlobAssetReference<NavMap> m_map;
    public static BlobAssetReference<NavMap> Map
    {
        get
        {
            if (!m_map.IsCreated) Debug.LogError("Requested NavMap BlobAsset when it was not yet created");
            return m_map;
        }
    }

    private NavMapAuthoring m_toConvert = null;

    public void AddForConversion(NavMapAuthoring navMapAuthoring)
    {
        m_toConvert = navMapAuthoring;
    }

    protected override void OnUpdate()
    {
        if (m_toConvert != null)
        {
            Convert();
            Debug.LogFormat("Converted {0} NavPlanes into blob asset", Map.Value.planes.Length);
        }
    }

    private void Convert()
    {
        NavPlaneAuthoring[] planesAuthoring = m_toConvert.GetComponentsInChildren<NavPlaneAuthoring>();
        Dictionary<NavPlaneAuthoring, int> planeIDs = new Dictionary<NavPlaneAuthoring, int>();

        for (int i = 0; i < planesAuthoring.Length; i++)
        {
            planeIDs.Add(planesAuthoring[i], i);
        }

        BlobBuilder blobBuilder = new BlobBuilder(Allocator.Temp);
        ref NavMap mapBlobAsset = ref blobBuilder.ConstructRoot<NavMap>();

        mapBlobAsset.settings = BakeBoidSettings();

        BlobBuilderArray<NavPlane> planesBlobArray = BakePlanesArray(blobBuilder, ref mapBlobAsset, planesAuthoring);

        for (int i = 0; i < planesBlobArray.Length; i++)
        {
            ref NavPlane plane = ref planesBlobArray[i];
            NavPlaneAuthoring planeAuthoring = planesAuthoring[i];

            List<NavPortal> portalsUp = new List<NavPortal>();
            List<NavPortal> portalsDown = new List<NavPortal>();
            List<NavPortal> portalsLeft = new List<NavPortal>();
            List<NavPortal> portalsRight = new List<NavPortal>();

            for (int j = 0; j < planeAuthoring.portals.Length; j++)
            {
                NavPortalAuthoring portalAuthoring = planeAuthoring.portals[j];
                NavPortal portal = new NavPortal();

                portal.wallOnThisPlane = portalAuthoring.wallOnThisPlane;
                portal.positionOnThisPlane = portalAuthoring.positionOnThisPlane;
                portal.positionOnTargetPlane = portalAuthoring.positionOnTargetPlane;
                portal.length = portalAuthoring.length;

                if (portalAuthoring.target != null)
                {
                    portal.targetPlaneID = planeIDs[portalAuthoring.target];
                } else
                {
                    Debug.LogWarningFormat("Portal on {0} missing a link to another NavPlane", planeAuthoring);
                    portal.targetPlaneID = -1;
                }

                switch(portal.wallOnThisPlane)
                {
                    case Wall.UP:
                        portalsUp.Add(portal);
                        break;
                    case Wall.DOWN:
                        portalsDown.Add(portal);
                        break;
                    case Wall.LEFT:
                        portalsLeft.Add(portal);
                        break;
                    case Wall.RIGHT:
                        portalsRight.Add(portal);
                        break;
                }
            }

            blobBuilder.Construct(ref plane.portalsUp, portalsUp.ToArray());
            blobBuilder.Construct(ref plane.portalsDown, portalsDown.ToArray());
            blobBuilder.Construct(ref plane.portalsLeft, portalsLeft.ToArray());
            blobBuilder.Construct(ref plane.portalsRight, portalsRight.ToArray());
        }

        m_map = blobBuilder.CreateBlobAssetReference<NavMap>(Allocator.Persistent);

        m_toConvert = null;

        blobBuilder.Dispose();
    }

    private BoidSettings BakeBoidSettings()
    {
        return new BoidSettings
        {
            cellSize = m_toConvert.cellSize,
            neighborAvoidanceRadius = m_toConvert.neighborAvoidanceRadius,
            wallAvoidanceDistance = m_toConvert.wallAvoidanceDistance,
            alignmentWeight = m_toConvert.alignmentWeight,
            avoidanceWeight = m_toConvert.avoidanceWeight,
            cohesionWeight = m_toConvert.cohesionWeight,
            wallAvoidanceWeight = m_toConvert.wallAvoidanceWeight
        };
    }

    private BlobBuilderArray<NavPlane> BakePlanesArray(BlobBuilder blobBuilder, ref NavMap mapBlobAsset, NavPlaneAuthoring[] planesAuthoring)
    {
        NavPlane[] planesTempArray = new NavPlane[planesAuthoring.Length];
        for (int i = 0; i < planesTempArray.Length; i++)
        {
            NavPlaneAuthoring planeAuthoring = planesAuthoring[i];
            NavPlane plane = new NavPlane();
            plane.position = planeAuthoring.planePosition;
            plane.rotation = planeAuthoring.planeRotation;
            plane.width = planeAuthoring.width;
            plane.height = planeAuthoring.height;
            plane.planeToWorld = float4x4.TRS(planeAuthoring.planePosition, planeAuthoring.planeRotation, math.float3(1, 1, 1));

            planesTempArray[i] = plane;
        }
        return blobBuilder.Construct(ref mapBlobAsset.planes, planesTempArray);
    }
}