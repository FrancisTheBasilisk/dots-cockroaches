﻿using System.Diagnostics;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

//This system transforms a 2D position on a plane into a 3D world positions

[UpdateInGroup(typeof(TransformSystemGroup))]
public class NavMapTransformSystem : SystemBase
{
    protected override void OnUpdate()
    {
        BlobAssetReference<NavMap> map = NavMapSystem.Map;

        Entities.WithNone<NonUniformScale>().ForEach((ref LocalToWorld localToWorld, in PlanarPosition planarPosition, in PlanarHeading planarHeading) =>
        {
            localToWorld.Value = CalculateTransformMatrix(in map, in planarPosition, in planarHeading, new float3(1, 1, 1));
        }).ScheduleParallel();

        Entities.ForEach((ref LocalToWorld localToWorld, in PlanarPosition planarPosition, in PlanarHeading planarHeading, in NonUniformScale scale) =>
        {
            localToWorld.Value = CalculateTransformMatrix(in map, in planarPosition, in planarHeading, scale.Value);
        }).ScheduleParallel();
    }

    private static float4x4 CalculateTransformMatrix(in BlobAssetReference<NavMap> map, in PlanarPosition planarPosition, in PlanarHeading planarHeading, float3 scale)
    {
        ref float4x4 planeToWorld = ref map.Value.planes[planarPosition.planeID].planeToWorld;
        float3 worldPosition = planeToWorld.MultiplyPoint(planarPosition.Value);
        float3 worldHeading = planeToWorld.MultiplyVector(planarHeading.Value);
        quaternion worldRotation = quaternion.LookRotation(worldHeading, -planeToWorld.c2.xyz);

        return float4x4.TRS(worldPosition, worldRotation, scale);
    }
}