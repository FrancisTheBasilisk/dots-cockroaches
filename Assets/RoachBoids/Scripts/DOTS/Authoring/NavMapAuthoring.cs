﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using UnityEngine;
using System.Collections.Generic;

//This is an authoring component for the NavMap. Its data will be converted into a BlobAsset at runtime
//Put this script on an empty GameObject

[DisallowMultipleComponent]
public class NavMapAuthoring : MonoBehaviour
{
    public bool drawCellGrid = true;
    public bool drawPortals = true;

    [Space]
    public float cellSize = 1; //How big is a square cell in which all boids are considered neighbors?
    public float neighborAvoidanceRadius = 0.5f;
    public float wallAvoidanceDistance = 0.5f;

    [Space]
    public float cohesionWeight = 3; //The weight of boids' cohesion behaviour
    public float alignmentWeight = 8; //The weight of boids' alignment behaviour
    public float avoidanceWeight = 8; //The weight of boids' neighbor avoidance behaviour
    public float wallAvoidanceWeight = 20; //The weight of boids' wall avoidance behaviour

    private void Awake()
    {
        World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<NavMapSystem>().AddForConversion(this);
    }
}