﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

//This is an authoring component for a single plane of the NavMap. Its data will be converted into a BlobAsset at runtime
//Put this script on a child GameObject of NavMapAuthoring

[DisallowMultipleComponent]
public class NavPlaneAuthoring : MonoBehaviour
{
    #region mainData
    
    //World position of the plane's bottom-left corner
    public Vector3 planePosition
    {
        get
        {
            return transform.position - transform.TransformVector(Offset);
        }
    }

    //World rotation of the plane
    public Quaternion planeRotation { get => transform.rotation; }

    public float width;
    public float height;

    public NavPortalAuthoring[] portals;
    #endregion

    #region authoring
    //This region of code is for only used for authoring and does not play a role in runtime

    //What part of the plane is considered the authoring GameObject's transform?
    public QuadPosition pivotPosition = QuadPosition.Center;

    //NavMapAuthoring component from the parent GameObject
    private NavMapAuthoring m_parentMap;
    private NavMapAuthoring GetParentMap()
    {
        if (m_parentMap == null) m_parentMap = GetComponentInParent<NavMapAuthoring>();
        if (m_parentMap == null) Debug.LogError("NavPlaneAuthoring GameObject should be a child of a NavMapAuthoring GameObject");
        return m_parentMap;
    }

    //Change the pivot position and calculate the new transfom position, so that the plane stays in the same place
    public void ChangePivotPosition(QuadPosition newPivotPosition)
    {
        Vector3 difference = newPivotPosition.GetOffset() - pivotPosition.GetOffset();
        difference.x *= width;
        difference.y *= height;

        transform.position += transform.TransformDirection(difference);

        pivotPosition = newPivotPosition;
    }

    //Where is the pivot position in local space, counting from the bottom-left corner of the plane?
    private Vector3 Offset
    {
        get
        {
            Vector3 offset = pivotPosition.GetOffset();
            offset.x *= width;
            offset.y *= height;
            return offset;
        }
    }

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR
        bool selected = Selection.activeGameObject == gameObject;

        NavMapAuthoring map = GetParentMap();
        bool drawCellGrid = map != null && map.drawCellGrid;
        bool drawPortals = map != null && map.drawPortals;

        //Draw the plane gizmo
        Gizmos.color = selected ? GizmoUtility.activeGizmoColor : GizmoUtility.inactiveGizmoColor;
        Gizmos.DrawMesh(GizmoUtility.Quad, planePosition, planeRotation, new Vector3(width, height, 0));
        if(drawCellGrid) GizmoUtility.DrawGrid(planePosition, planeRotation, width, height, map.cellSize);

        //Draw portals
        if (selected || drawPortals)
        {
            foreach (NavPortalAuthoring portal in portals)
            {
                if (portal.length <= 0) return;

                Vector2 localPortalStart = PortalUtility.GetPortalPositionOnPlane(portal.wallOnThisPlane, portal.positionOnThisPlane, width, height);
                Vector2 localPortalEnd = PortalUtility.GetPortalPositionOnPlane(portal.wallOnThisPlane, portal.positionOnThisPlane + portal.length, width, height);

                Handles.DrawAAPolyLine(GizmoUtility.LocalPortalLineTexture, To3DPosition(localPortalStart), To3DPosition(localPortalEnd));

                if (selected && portal.target != null)
                {
                    Wall opposite = portal.wallOnThisPlane.GetOpposite();
                    Vector2 targetPortalStart = PortalUtility.GetPortalPositionOnPlane(opposite, portal.positionOnTargetPlane, portal.target.width, portal.target.height);
                    Vector2 targetPortalEnd = PortalUtility.GetPortalPositionOnPlane(opposite, portal.positionOnTargetPlane + portal.length, portal.target.width, portal.target.height);

                    Handles.DrawAAPolyLine(GizmoUtility.RemotePortalLineTexture, portal.target.To3DPosition(targetPortalStart), portal.target.To3DPosition(targetPortalEnd));
                }
            }
        }
#endif
    }
    private Vector3 To3DPosition(Vector2 position2d)
    {
        return planePosition + transform.TransformDirection(position2d);
    }
    #endregion
}

[System.Serializable]
public class NavPortalAuthoring
{
    public Wall wallOnThisPlane; //the wall on which this portal is located
    public float positionOnThisPlane; //1-dimensional position on this plane's wall
    public float positionOnTargetPlane; //1-dimensional position on the wall of a target plane
    public float length; //1D-end-position - 1D-start-position = length
    public NavPlaneAuthoring target; //Authoring component of the target plane
}